    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>
    
    <!-- Search model -->
	<div class="search-model">
		<div class="h-100 d-flex align-items-center justify-content-center">
			<div class="search-close-switch">+</div>
			<form class="search-model-form">
				<input type="text" id="search-input" placeholder="Search here.....">
			</form>
		</div>
	</div>
	<!-- Search model end -->
    <!-- Header Section Begin -->
    <header class="header-section">
        <div class="container-fluid">
            <div class="inner-header">
                <div class="logo">
                    <a href="./index.html"><img src="img/logo.png" alt=""></a>
                </div>
                <div class="header-right">
                    <img src="img/icons/search.png" alt="" class="search-trigger">
                    <img src="img/icons/man.png" alt="">
                    <a href="#">
                        <img src="img/icons/bag.png" alt="">
                        <span>2</span>
                    </a>
                </div>
                <div class="user-access">
                    <a data-toggle="modal" data-target="#register">Register</a>
                    <a class="in" data-toggle="modal" data-target="#login">Sign in</a>
                </div>
                <nav class="main-menu mobile-menu">
                    <ul>
                        <li><a href="?page=Home">Home</a></li>
                        <li><a href="?page=Shop">Shop</a>
                            <ul class="sub-menu">
                                <li><a href="?page=About">Product Page</a></li>
                                <li><a href="?page=Shopping">Shopping Card</a></li>
                                <li><a href="?page=Check">Check out</a></li>
                            </ul>
                        </li>
                        <li><a href="?page=About">About</a></li>
                        <li><a href="?page=Blog">Blog</a></li>
                        <li><a href="?page=Contact">Contact</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>
    <!-- Header Info Begin -->
    <div class="header-info">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <div class="header-item">
                        <img src="img/icons/delivery.png" alt="">
                        <p>Free shipping on orders over $30 in USA</p>
                    </div>
                </div>
                <div class="col-md-4 text-left text-lg-center">
                    <div class="header-item">
                        <img src="img/icons/voucher.png" alt="">
                        <p>20% Student Discount</p>
                    </div>
                </div>
                <div class="col-md-4 text-left text-xl-right">
                    <div class="header-item">
                    <img src="img/icons/sales.png" alt="">
                    <p>30% off on dresses. Use code: 30OFF</p>
                </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Header Info End -->
    <!-- Header End -->


    <!-- Login -->
    <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title w-100" id="myModalLabel">Sign in</h4>
            </div>
            <form action="contact.html" method="POST">
            <div class="modal-body">
                
                <input class="form-control" type="text" name="user" required="" placeholder="user">
                <input class="form-control" type="Password" name="pass" required="" placeholder="Password">
                
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-primary" value="Sign in"></input>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end login -->

<!-- Register -->
    <div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title w-100" id="myModalLabel">Register</h4>
            </div>
            <form action="contact.html" method="POST">
            <div class="modal-body">
                
                <input class="form-control" type="text" name="name" required="" placeholder="Nombre">
                <input class="form-control" type="text" name="surname" required="" placeholder="Apellido">
                <input class="form-control" type="email" name="email" required="" placeholder="Correo electronico">
                <input class="form-control" type="number" name="phone" required="" placeholder="Teléfono">
                <input class="form-control" type="text" name="address" required="" placeholder="dirección">

                <input class="form-control" type="Password" name="pass" required="" placeholder="Contraseña">
                <input class="form-control" type="Password" name="conpass" required="" placeholder="Confirmación de la contraseña">

                <label>Profesor</label>
                <input type="radio" name="type" checked="">
                <label>Alumno</label>
                <input type="radio" name="type">
                
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-primary" value="Sign in"></input>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end register -->